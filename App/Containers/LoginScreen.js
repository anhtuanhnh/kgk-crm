import React, { Component } from 'react'
import { ScrollView,View, Text, KeyboardAvoidingView , Dimensions, StyleSheet} from 'react-native'
import { connect } from 'react-redux'
// Add Actions - replace 'Your' with whatever your reducer is called :)
// import YourActions from '../Redux/YourRedux'
import Logo from '../Components/Logo'
import Form from '../Components/Form'
import Wallpaper from '../Components/Wallpaper'
import ButtonSubmit from '../Components/ButtonSubmit'
import SignupSection from '../Components/SignupSection'
// Styles

import styles from './Styles/LoginScreenStyle'

class LoginScreen extends Component {
  render () {
    console.log(this.props)

    return (
      <View style={styles.mainContainer}>
        <ScrollView style={styles.container}>
          <Wallpaper>
            <Logo />
            <Form />
            <SignupSection />
            <ButtonSubmit navigation={this.props.navigation}/>
          </Wallpaper>
        </ScrollView>
      </View>
    )
  }
}

const mapStateToProps = (state) => {
  return {
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(LoginScreen)
